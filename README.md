<h1 align="center">
  InstaMenu Extras
</h1>

**For Arch** it can be founded in [AUR](https://aur.archlinux.org/packages/instawm-schemas)

**For Gentoo** it can be found at [Release](https://github.com/The-Repo-Club/instawm-schemas/releases/latest)
